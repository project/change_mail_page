<?php

namespace Drupal\change_mail_page\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Password\PasswordInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a separate mail change form for the current user.
 */
class ChangeMailForm extends FormBase {
  /**
   * The Password Hasher.
   *
   * @var \Drupal\Core\Password\PasswordInterface
   */
  protected $passwordHasher;

  /**
   * Constructs a UserPasswordForm object.
   *
   * @param \Drupal\Core\Password\PasswordInterface $password_hasher
   *   The password service.
   * @param \Drupal\Core\Config\Config $user_settings
   *   The user settings.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(PasswordInterface $password_hasher, Config $user_settings, ModuleHandlerInterface $module_handler) {
    $this->passwordHasher = $password_hasher;
    $this->userSettings = $user_settings;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('password'),
      $container->get('config.factory')->get('user.settings'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'change_mail_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\user\UserInterface $user
   *   The user object.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    $account = $user;

    $form['#cache']['tags'] = $this->userSettings->getCacheTags();

    $form['account'] = [
      '#type'   => 'container',
    ];
    $form['account']['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      // Do not let web browsers remember this password, since we are
      // trying to confirm that the person submitting the form actually
      // knows the current one.
      '#attributes' => ['autocomplete' => 'off'],
      '#description' => $this->t('A valid email address. All emails from the system will be sent to this address.
       The email address is not made public and will only be used if you wish to receive a new
        password or wish to receive certain news or
        notifications by email.'),
      '#required' => TRUE,
      '#default_value' => $account->getEmail(),
    ];

    $form['account']['current_pass'] = [
      '#type' => 'password',
      '#title' => $this->t('Current password'),
      '#size' => 25,
      '#required' => TRUE,
      // Do not let web browsers remember this password, since we are
      // trying to confirm that the person submitting the form actually
      // knows the current one.
      '#attributes' => ['autocomplete' => 'off'],
      '#description' => $this->t('Required if you want to change the %mail.', [
        '%mail' => $form['account']['mail']['#title'],
      ]),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    $form_state->set('user', $account);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $account = $form_state->get('user');

    $mail = trim($form_state->getValue('mail'));
    $current_pass = trim($form_state->getValue('current_pass'));

    $account->setEmail($mail);
    $account->setExistingPassword($current_pass);

    $violations = $account->validate();

    if ($violations->count()) {
      /*
       * Any required profile fields will get added to our violations.
       * Let's just check the fields we are interested in.
       */
      foreach ($violations->getByFields(['mail', 'current_pass']) as $violation) {
        $form_state->setErrorByName($violation->getPropertyPath(), $violation->getMessage());
      }
    }

    /* check dns module */
    $moduleHandler = $this->moduleHandler;
    if ($moduleHandler->moduleExists('check_dns')) {
      check_dns_user_register_validate($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $account = $form_state->get('user');

    $mail = trim($form_state->getValue('mail'));
    $current_pass = trim($form_state->getValue('current_pass'));

    $account->setEmail($mail);
    $account->setExistingPassword($current_pass);

    $account->save();

    $this->messenger()->addStatus($this->t('Your email has been changed.'));

    // @todo Should we enable this?
    //   $form_state->setRedirect('user.page');.
  }

}
